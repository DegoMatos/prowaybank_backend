package prowaybank.tests;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import prowaybank.controller.rendimentoInvestimento;
import prowaybank.model.InvestimentoExterno;

class rendimentoInvestimentoTest {

	@Test
	void consultaTodosInvestimentoRetornaArrayComTodosInvestimentos() {
		
		rendimentoInvestimento ControllerRendimento = new rendimentoInvestimento();
		
		InvestimentoExterno inv1 = new InvestimentoExterno();
		inv1.setEmpresa("Notre Dame Part (GNDI3)");
		inv1.setSetor("S�ude");
		inv1.setValorInvestido(1620.2);
		inv1.setVariacao(5.79);
		inv1.setDataInvestimento(LocalDate.of(2021, 01, 12));
		
		InvestimentoExterno inv2 = new InvestimentoExterno();
		inv2.setEmpresa("Ultrapar (UGPA3)");
		inv2.setSetor("Petroqu�mico");
		inv2.setValorInvestido(268.2);
		inv2.setVariacao(-11.64);
		inv2.setDataInvestimento(LocalDate.of(2021, 05, 15));
		
		ArrayList<InvestimentoExterno> todosInvestimentos =  new ArrayList<InvestimentoExterno>();
		todosInvestimentos.add(inv1);
		todosInvestimentos.add(inv2);
		
		InvestimentoExterno[] resultado = ControllerRendimento.consultaInvestimentosExternos(todosInvestimentos);
		
		InvestimentoExterno[] referencia = {inv1, inv2};
		
		Assert.assertArrayEquals(referencia, resultado);
	}
	
	@Test
	void calculaRendimentoTotalInvestidoRetornaDoubleValorTotalInvestido() {
		
		rendimentoInvestimento ControllerRendimento = new rendimentoInvestimento();
		
		InvestimentoExterno inv1 = new InvestimentoExterno();
		inv1.setEmpresa("Notre Dame Part (GNDI3)");
		inv1.setSetor("S�ude");
		inv1.setValorInvestido(1620.2);
		inv1.setVariacao(5.79);
		inv1.setDataInvestimento(LocalDate.of(2021, 01, 12));
		
		InvestimentoExterno inv2 = new InvestimentoExterno();
		inv2.setEmpresa("Ultrapar (UGPA3)");
		inv2.setSetor("Petroqu�mico");
		inv2.setValorInvestido(268.2);
		inv2.setVariacao(-11.64);
		inv2.setDataInvestimento(LocalDate.of(2021, 05, 15));
		
		ArrayList<InvestimentoExterno> todosInvestimentos =  new ArrayList<InvestimentoExterno>();
		todosInvestimentos.add(inv1);
		todosInvestimentos.add(inv2);
		
		double valorTotal = ControllerRendimento.rendimentoTotal(todosInvestimentos);
		
		Assert.assertEquals(62.59110000000001, valorTotal, 0);
		
	}
	
	@Test
	void salvandoValorTotalNoBancoDeDados() {
		
		rendimentoInvestimento ControllerRendimento = new rendimentoInvestimento();
		
		double resultado = ControllerRendimento.salvarRendimentoTotal(62.59110000000001);
		
		Assert.assertEquals(62.59110000000001, resultado, 0);
		
	}
	
	@Test
	void calcularRendimentoCadaInvestimentoRetornaArrayComRendimentoCalculadoSeparadoPorData() {
		
		rendimentoInvestimento ControllerRendimento = new rendimentoInvestimento();
		
		InvestimentoExterno inv1 = new InvestimentoExterno();
		inv1.setEmpresa("Notre Dame Part (GNDI3)");
		inv1.setSetor("S�ude");
		inv1.setValorInvestido(1620.2);
		inv1.setVariacao(5.79);
		inv1.setDataInvestimento(LocalDate.of(2021, 01, 12));
		
		InvestimentoExterno inv2 = new InvestimentoExterno();
		inv2.setEmpresa("Ultrapar (UGPA3)");
		inv2.setSetor("Petroqu�mico");
		inv2.setValorInvestido(268.2);
		inv2.setVariacao(-11.64);
		inv2.setDataInvestimento(LocalDate.of(2021, 05, 15));
		
		ArrayList<InvestimentoExterno> todosInvestimentos =  new ArrayList<InvestimentoExterno>();
		todosInvestimentos.add(inv1);
		todosInvestimentos.add(inv2);
		
		ArrayList<InvestimentoExterno> resultado = new ArrayList<InvestimentoExterno>();
		resultado = ControllerRendimento.rendimentoParticular(todosInvestimentos);
		
		ArrayList<InvestimentoExterno> referencia = new ArrayList<InvestimentoExterno>();
		
		inv1.getRendimento();
		inv2.getRendimento();
		
		referencia.add(inv1);
		referencia.add(inv2);
		
		Assert.assertEquals(referencia, resultado);
		
	}
	
	@Test
	void buscaInvestimentoDeMesesEspecificosRetornaArrayComInvestimentosDoDeterminadoMes() {
		
		rendimentoInvestimento ControllerRendimento = new rendimentoInvestimento();
		
		InvestimentoExterno inv1 = new InvestimentoExterno();
		inv1.setEmpresa("Notre Dame Part (GNDI3)");
		inv1.setSetor("S�ude");
		inv1.setValorInvestido(1620.2);
		inv1.setVariacao(5.79);
		inv1.setDataInvestimento(LocalDate.of(2021, 01, 12));
		
		InvestimentoExterno inv2 = new InvestimentoExterno();
		inv2.setEmpresa("Ultrapar (UGPA3)");
		inv2.setSetor("Petroqu�mico");
		inv2.setValorInvestido(268.2);
		inv2.setVariacao(-11.64);
		inv2.setDataInvestimento(LocalDate.of(2021, 05, 15));
		
		ArrayList<InvestimentoExterno> todosInvestimentos =  new ArrayList<InvestimentoExterno>();
		todosInvestimentos.add(inv1);
		todosInvestimentos.add(inv2);
		
		List<InvestimentoExterno> resultado = new ArrayList<InvestimentoExterno>();
		resultado = ControllerRendimento.rendimentoMesEspecifico(todosInvestimentos, 1);
		
		List<InvestimentoExterno> referencia = new ArrayList<InvestimentoExterno>();
		referencia.add(inv1);
		
		Assert.assertEquals(referencia, resultado);
	}
	
	@Test
	void salvandoArrayDadosBancoDeDadosRetornaArrayDados() {
		
		rendimentoInvestimento ControllerRendimento = new rendimentoInvestimento();
		
		InvestimentoExterno inv1 = new InvestimentoExterno();
		inv1.setEmpresa("Notre Dame Part (GNDI3)");
		inv1.setSetor("S�ude");
		inv1.setValorInvestido(1620.2);
		inv1.setVariacao(5.79);
		inv1.setDataInvestimento(LocalDate.of(2021, 01, 12));
		
		InvestimentoExterno inv2 = new InvestimentoExterno();
		inv2.setEmpresa("Ultrapar (UGPA3)");
		inv2.setSetor("Petroqu�mico");
		inv2.setValorInvestido(268.2);
		inv2.setVariacao(-11.64);
		inv2.setDataInvestimento(LocalDate.of(2021, 05, 15));
		
		ArrayList<InvestimentoExterno> todosInvestimentos =  new ArrayList<InvestimentoExterno>();
		todosInvestimentos.add(inv1);
		todosInvestimentos.add(inv2);
		
		ArrayList<InvestimentoExterno> resultado = new ArrayList<InvestimentoExterno>();
		
		
		ArrayList<InvestimentoExterno> referencia = new ArrayList<InvestimentoExterno>();
		referencia = todosInvestimentos;
		
		resultado = ControllerRendimento.salvarRendimento(todosInvestimentos);
		
		Assert.assertEquals(referencia, resultado);
	}
	
}
