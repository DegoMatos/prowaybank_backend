package prowaybank.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import prowaybank.controller.SimulacaoInvestimentoController;
import prowaybank.enums.TipoInvestimentoEnum;
import prowaybank.enums.TipoTaxaEnum;
import prowaybank.model.SimulacaoInvestimento;

class SimulacaoInvestimentoControllerTest {

	@Test
	void simularInvestimentoTrue() {
		SimulacaoInvestimento simulacaoInvestimento = new SimulacaoInvestimento(TipoInvestimentoEnum.LCI,
				TipoTaxaEnum.POS, 100.0, 12);
		SimulacaoInvestimentoController simulacaoInvestimentoController = new SimulacaoInvestimentoController(
				simulacaoInvestimento);
		Double resultado = simulacaoInvestimentoController.simularInvestimento();
		assertEquals(143.41, resultado, 0.1);
	}


}
