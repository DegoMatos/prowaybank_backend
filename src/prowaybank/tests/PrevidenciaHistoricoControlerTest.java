package prowaybank.tests;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import prowaybank.controller.PrevidenciaHistoricoControler;
import prowaybank.model.Previdencia;

class PrevidenciaHistoricoControlerTest {
	
	/*Testes do primeiro metodo calcularAnosInvestimentoSemJuros()*/
	
	@Test
	void calcularAnosInvestimentoSemJurosRetornaDouble() {	
		Previdencia previdencia = new Previdencia();
		
		previdencia.setIdadeAposentadoria(60); 
		previdencia.setIdadeIncial(30);
		previdencia.setInvestimentoMensal(1000);
		
		PrevidenciaHistoricoControler previdenciaHistorico = new PrevidenciaHistoricoControler(previdencia);
		//previdenciaHistorico.prev = previdencia;
		
		
		double resultadoFinal = previdenciaHistorico.calcularAnosInvestimentoSemJuros();	
		Assert.assertEquals(360000, resultadoFinal, 0000.9);
	} 
	
	@Test
	void calcularAnosInvestimentoIdadeInicialInferior18() {
		Previdencia previdencia = new Previdencia();
		
		previdencia.setIdadeAposentadoria(60);
		previdencia.setIdadeIncial(17);
		previdencia.setInvestimentoMensal(1000);
		
		PrevidenciaHistoricoControler previdenciaHistorico = new PrevidenciaHistoricoControler(previdencia);
		//previdenciaHistorico.prev = previdencia;
		
		
		double resultadoFinal = previdenciaHistorico.calcularAnosInvestimentoSemJuros();	
		Assert.assertNotEquals(360000, resultadoFinal, 0000.9);
	}
	
	@Test
	void calcularAnosInvestimentoTempoMinimoContribuicaoInferiorDefault() {
		Previdencia previdencia = new Previdencia();
		
		previdencia.setIdadeAposentadoria(50);
		previdencia.setIdadeIncial(30);
		previdencia.setInvestimentoMensal(1000);
		
		PrevidenciaHistoricoControler previdenciaHistorico = new PrevidenciaHistoricoControler(previdencia);
		//previdenciaHistorico.prev = previdencia;
		
		
		double resultadoFinal = previdenciaHistorico.calcularAnosInvestimentoSemJuros();	
		Assert.assertNotEquals(360000, resultadoFinal, 0000.9);
	}
	
	@Test
	void calcularAnosInvestimentoInvestimentoMensalAbaixoDoValorMinimoSolicitado() {
		Previdencia previdencia = new Previdencia();
		
		previdencia.setIdadeAposentadoria(60);
		previdencia.setIdadeIncial(30);
		previdencia.setInvestimentoMensal(80);
		
		PrevidenciaHistoricoControler previdenciaHistorico = new PrevidenciaHistoricoControler(previdencia);
		//previdenciaHistorico.prev = previdencia;
		
		
		double resultadoFinal = previdenciaHistorico.calcularAnosInvestimentoSemJuros();	
		Assert.assertNotEquals(360000, resultadoFinal, 0000.9);
	}
	
	
	/*Testes do segundo metodo calcularAnosInvestimentoSemJuros()*/
	
	@Test
	void verificarTotalInvestimentoAnosComJuros() {
		Previdencia previdencia = new Previdencia();
		
		//previdenciaHistorico.prev = previdencia;
		
		previdencia.setInvestimentoMensal(1000);
		previdencia.setValorInvestidoAnos(previdencia.getInvestimentoMensal()*12);
		
		PrevidenciaHistoricoControler previdenciaHistorico = new PrevidenciaHistoricoControler(previdencia);
		
		double[] result = previdenciaHistorico.verificarTotalInvestimentoAnosComJuros();
		Assert.assertArrayEquals(result, result, 0);
		
		
	}
}
