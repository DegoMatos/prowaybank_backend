package prowaybank.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import prowaybank.controller.AporteController;
import prowaybank.model.Aporte;
import prowaybank.model.Usuario;

class AporteTest {

	// Validar nome completo
	@Test
	void validarNomeCompletoRetornaTrue() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("Ana");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.verificaNomeCompleto());
	}

	@Test
	void validarNomeCompletoRetornaFalse() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("Eduarda");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.verificaNomeCompleto());
	}

	// Nome completo n�o pode ficar em branco
	@Test
	void validarNomeCompletoVazioRetornaFalse() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.nomeCompletoVazio());
	}

	@Test
	void validarNomeCompletoVazioRetornaTrue() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("Flavia");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.nomeCompletoVazio());
	}

	// Validar nome do benefici�rio
	@Test
	void validarNomeBeneficiarioRetornaTrue() {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Yuri");
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.verificarNomeBeneficiario());
	}

	@Test
	void validarNomeBeneficiarioRetornaFalse() {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Fernanda");
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.verificarNomeBeneficiario());
	}

	// Nome do benefici�rio n�o pode ficar vazio
	@Test
	void validarNomeBeneficiarioVazioRetornaFalse() {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("");
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.nomeBeneficiarioVazio());

	}

	@Test
	void validarNomeBeneficiarioVazioRetornaTrue() {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Yuri");
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.nomeBeneficiarioVazio());

	}

	// Validar valor de outro plano de previd�ncia
	@Test
	void validarOutraPrevidenciaRetornaFalse() {
		Aporte aporte = new Aporte();
		aporte.setValorOutro(null);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.outraPrevidencia());

	}

	@Test
	void validarOutraPrevidenciaRetornaTrue() {
		Aporte aporte = new Aporte();
		aporte.setValorOutro("350");
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.outraPrevidencia());

	}

	// Validar renda mensal
	@Test
	void validarRendaMensalRetornaFalse() {
		Aporte aporte = new Aporte();
		aporte.setRendaMensal(0);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.rendaMensal());

	}

	@Test
	void validarRendaMensalRetornaTrue() {
		Aporte aporte = new Aporte();
		aporte.setRendaMensal(10);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.rendaMensal());

	}

	// Validar valor mensal
	@Test
	void validarValorMensalRetornaFalse() {
		Aporte aporte = new Aporte();
		aporte.setValorAplicado(0);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.valorMensal());

	}

	@Test
	void validarValorMensalRetornaTrue() {
		Aporte aporte = new Aporte();
		aporte.setValorAplicado(10);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.valorMensal());

	}

	// Data de nascimento n�o aceita datas falsas
	@Test
	void validarDataRetornaTrue() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setDataNascimento("12/02/2003");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.data());

	}

	@Test
	void validarDataRetornaFalse() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setDataNascimento("31/02/2003");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.data());

	}

	// Data de nascimento n�o aceita datas menor que hoje
	@Test
	void validarDataMenorQueHojeRetornaTrue() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setDataNascimento("31/12/2003");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.dataMenorQueHoje());

	}

	@Test
	void validarDataMenorQueHojeRetornaFalse() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setDataNascimento("01/12/2023");
		aporte.setUsuario(usuario);
		;
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.dataMenorQueHoje());

	}

	// Campo de sele��o do sexo n�o pode ficar vazio
	@Test
	void validarSexoVazioRetornaFalse() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setSexo("");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.sexoVazio());

	}

	@Test
	void validarSexoVazioRetornaTrue() {
		Aporte aporte = new Aporte();

		Usuario usuario = new Usuario();
		usuario.setSexo("Feminino");
		aporte.setUsuario(usuario);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.sexoVazio());

	}

	// Validar resgate �nico.
	@Test
	void validarResgateUnicoRetornaFalse() {
		Aporte aporte = new Aporte();
		aporte.setResgateUnico(0);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.resgateUnico());

	}

	@Test
	void validarResgateUnicoRetornaTrue() {
		Aporte aporte = new Aporte();
		aporte.setResgateUnico(500);
		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.resgateUnico());

	}

	@Test
	void validaDadosAporteTrue() {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Yuri");
		aporte.setValorOutro("350");
		aporte.setRendaMensal(10);
		aporte.setValorAplicado(10);
		aporte.setResgateUnico(500);

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("Ana");
		usuario.setSexo("Feminino");
		usuario.setDataNascimento("12/02/2003");
		aporte.setUsuario(usuario);

		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertTrue(controllerAporte.verificaDadosAporte());
	}

	@Test
	void validaDadosAporteFalse() {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Yuri");
		aporte.setValorOutro("350");
		aporte.setRendaMensal(10);
		aporte.setValorAplicado(10);
		aporte.setResgateUnico(500);

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("setNomeCompleto(\"Eduarda\");");
		usuario.setSexo("Feminino");
		usuario.setDataNascimento("12/02/2003");
		aporte.setUsuario(usuario);

		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertFalse(controllerAporte.verificaDadosAporte());
	}

	@Test
	void salvaDadosCorretos() throws Exception {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Yuri");
		aporte.setValorOutro("350");
		aporte.setRendaMensal(10);
		aporte.setValorAplicado(10);
		aporte.setResgateUnico(500);

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("Ana");
		usuario.setSexo("Feminino");
		usuario.setDataNascimento("12/02/2003");
		aporte.setUsuario(usuario);

		ArrayList<Aporte> aportes = new ArrayList<Aporte>();

		AporteController controllerAporte = new AporteController(aporte);
		Assert.assertEquals(controllerAporte.salvarDados(aporte, aportes), aporte);
	}

	@Test
	void tentarSalvaDadosIncorretos() throws Exception {
		Aporte aporte = new Aporte();
		aporte.setNomeBeneficiario("Yuri");
		aporte.setValorOutro("350");
		aporte.setRendaMensal(10);
		aporte.setValorAplicado(10);
		aporte.setResgateUnico(500);

		Usuario usuario = new Usuario();
		usuario.setNomeCompleto("Eduarda");
		usuario.setSexo("Feminino");
		usuario.setDataNascimento("12/02/2003");
		aporte.setUsuario(usuario);

		ArrayList<Aporte> aportes = new ArrayList<Aporte>();

		AporteController controllerAporte = new AporteController(aporte);
		try {
			@SuppressWarnings("unused")
			Aporte teste = controllerAporte.salvarDados(aporte, aportes);
			fail("Excess�o n�o executada");
		} catch (Exception e) {
			assertEquals("Dados incorretos", e.getMessage());
		}
	}

	@Test
	void exibirAportes() {
		AporteController controllerAporte = new AporteController(new Aporte());
		List<Aporte> aportes = new ArrayList<Aporte>();
		aportes.add(new Aporte("Yuri", "350", 10, 10, 500, false, false, new Usuario("Ana", "12/02/2003", "Feminino")));
		aportes.add(
				new Aporte("Yuri", "450", 10, 10, 510, true, true, new Usuario("Lucio", "12/02/2004", "Masculino")));
		assertEquals(aportes, controllerAporte.buscarTodosAportes());
	}

	@Test
	void exibirAportesErro() {
		AporteController controllerAporte = new AporteController(new Aporte());
		List<Aporte> aportes = new ArrayList<Aporte>();
		aportes.add(new Aporte("Yuri", "350", 10, 10, 500, false, false, new Usuario("Ana", "12/02/2003", "Feminino")));
		aportes.add(
				new Aporte("Yuri", "450", 10, 10, 510, true, true, new Usuario("Lucio", "12/02/2004", "Masculino")));
		aportes.add(
				new Aporte("Lucas", "450", 10, 10, 510, true, true, new Usuario("Lucio", "12/02/2004", "Masculino")));
		assertNotEquals(aportes, controllerAporte.buscarTodosAportes());
	}

	@Test
	void buscarAportePorBeneficiarioTrue() {

		AporteController controllerAporte = new AporteController(new Aporte());
		List<Aporte> aportes = new ArrayList<Aporte>();

		aportes.add(new Aporte("Yuri", "350", 10, 10, 500, false, false, new Usuario("Ana", "12/02/2003", "Feminino")));
		aportes.add(
				new Aporte("Yuri", "450", 10, 10, 510, true, true, new Usuario("Lucio", "12/02/2004", "Masculino")));
		aportes.add(
				new Aporte("Lucas", "450", 10, 10, 510, true, true, new Usuario("Marco", "12/02/2004", "Masculino")));

		List<Aporte> aportesEncontrado = controllerAporte.buscarPorBeneficiario("Lucas", aportes);
		assertFalse(aportesEncontrado.isEmpty());
	}

	@Test
	void buscarAportePorBeneficiarioFalse() {

		AporteController controllerAporte = new AporteController(new Aporte());
		List<Aporte> aportes = new ArrayList<Aporte>();

		aportes.add(new Aporte("Yuri", "350", 10, 10, 500, false, false, new Usuario("Ana", "12/02/2003", "Feminino")));
		aportes.add(
				new Aporte("Yuri", "450", 10, 10, 510, true, true, new Usuario("Lucio", "12/02/2004", "Masculino")));
		aportes.add(
				new Aporte("Lucas", "450", 10, 10, 510, true, true, new Usuario("Marco", "12/02/2004", "Masculino")));

		List<Aporte> aportesEncontrado = controllerAporte.buscarPorBeneficiario("Marco", aportes);
		assertTrue(aportesEncontrado.isEmpty());
	}

}
