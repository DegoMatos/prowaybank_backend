package prowaybank.tests;


import org.junit.Assert;
import org.junit.jupiter.api.Test;

import prowaybank.controller.InvestimentoInvestirController;
import prowaybank.model.Investimento;

class InvestimentoInvestirTest {

	@Test
	void valorCorretoRecebeValorMinimo1000ValorInvestido1200RetornaTrue() {
		Investimento investir = new Investimento(); //model 
		InvestimentoInvestirController investcontroller = new InvestimentoInvestirController(investir); 
		investir.setValorMinimoInvestimento(1000); 
		investir.setValorInvestido(1200);
		boolean retorno = investcontroller.valorCorreto();
		Assert.assertTrue(retorno); 
	}
	
	

	@Test
	void valorCorretoRecebeValorMinimo1200ValorInvestido1000RetornaFalse() {
		Investimento investir = new Investimento(); //model 
		InvestimentoInvestirController investcontroller = new InvestimentoInvestirController(investir);
		investir.setValorMinimoInvestimento(1200);
		investir.setValorInvestido(1000);
		boolean retorno = investcontroller.valorCorreto();
		Assert.assertFalse(retorno); 
	}

	@Test
	void consultaSaldoRecebeValorInvestido1200Saldo1000RetornaFalse() {
		Investimento investir = new Investimento(); //model 
		InvestimentoInvestirController investcontroller = new InvestimentoInvestirController(investir);
		investir.setValorInvestido(1200);
		boolean retorno = investcontroller.consultaSaldo(1000);
		Assert.assertFalse(retorno);
	}
	
	@Test
	void consultaSaldoRecebeValorInvestido1000Saldo1200RetornaTrue() {
		Investimento investir = new Investimento(); //model 
		InvestimentoInvestirController investcontroller = new InvestimentoInvestirController(investir);
		investir.setValorInvestido(1000);
		boolean retorno = investcontroller.consultaSaldo(1200);
		Assert.assertTrue(retorno);
	}
	
	@Test
	void valorEstimadoRecebeValorInvestido1000TaxaInvsetimento2TempoInvestido12() {
		Investimento investir = new Investimento(); //model 
		InvestimentoInvestirController investcontroller = new InvestimentoInvestirController(investir);
		investir.setValorInvestido(1000);
		investir.setTaxaInvestimento(0.02f);
		investir.setTempoInvestimento(12);
		double resultado = investcontroller.valorEstimado();
		Assert.assertEquals(1268.24150997678, resultado, 0);
	}
	
	
} 


