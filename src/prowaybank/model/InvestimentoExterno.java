package prowaybank.model;

import java.time.LocalDate;

public class InvestimentoExterno {
	private String empresa;
	private String setor; 
	private double valorInvestido;
	private double variacao;
	private LocalDate dataInvestimento;
	private double rendimento;
    
	public double getRendimento() {
		return rendimento;
	}

	public void setRendimento(double rendimento) {
		this.rendimento = rendimento;
	}

	public String getEmpresa() {
		return empresa;
	}
	
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public String getSetor() {
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	public double getValorInvestido() {
		return valorInvestido;
	}
	public void setValorInvestido(double valorInvestido) {
		this.valorInvestido = valorInvestido;
	}
	public double getVariacao() {
		return variacao;
	}
	public void setVariacao(double variacao) {
		this.variacao = variacao;
	}
	public LocalDate getDataInvestimento() {
		return dataInvestimento;
	}
	public void setDataInvestimento(LocalDate dataInvestimento) {
		this.dataInvestimento = dataInvestimento;
	}

}

