package prowaybank.model;

import java.time.LocalDate;
import java.util.Objects;

public class Aporte implements IAplicacao{

	private String nomeBeneficiario;
	private String valorOutro;
	private double valorAplicado;
	private double rendaMensal;
	private double resgateUnico;
	private boolean peculio;
	private boolean pensao;
	private Usuario usuario;
	private LocalDate data;

	public Aporte() {
		super();
	}

	public Aporte(String nomeBeneficiario, String valorOutro, double valorAplicado, double rendaMensal,
			double resgateUnico, boolean peculio, boolean pensao, Usuario usuario) {
		super();
		this.nomeBeneficiario = nomeBeneficiario;
		this.valorOutro = valorOutro;
		this.valorAplicado = valorAplicado;
		this.rendaMensal = rendaMensal;
		this.resgateUnico = resgateUnico;
		this.peculio = peculio;
		this.pensao = pensao;
		this.usuario = usuario;
	}

	public String getNomeBeneficiario() {
		return nomeBeneficiario;
	}

	public void setNomeBeneficiario(String nomeBeneficiario) {
		this.nomeBeneficiario = nomeBeneficiario;
	}

	public String getValorOutro() {
		return valorOutro;
	}

	public void setValorOutro(String valorOutro) {
		this.valorOutro = valorOutro;
	}

	public double getValorAplicado() {
		return valorAplicado;
	}

	public void setValorAplicado(double valorAplicado) {
		this.valorAplicado = valorAplicado;
	}

	public double getRendaMensal() {
		return rendaMensal;
	}

	public void setRendaMensal(double rendaMensal) {
		this.rendaMensal = rendaMensal;
	}

	public double getResgateUnico() {
		return resgateUnico;
	}

	public void setResgateUnico(double resgateUnico) {
		this.resgateUnico = resgateUnico;
	}

	public boolean isPeculio() {
		return peculio;
	}

	public void setPeculio(boolean peculio) {
		this.peculio = peculio;
	}

	public boolean isPensao() {
		return pensao;
	}

	public void setPensao(boolean pensao) {
		this.pensao = pensao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		return Objects.hash(usuario);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Aporte other = (Aporte) obj;
		return Objects.equals(usuario, other.usuario);
	}

	@Override
	public String toString() {
		return "Aporte [nomeBeneficiario=" + nomeBeneficiario + ", valorOutro=" + valorOutro + ", valorAplicado="
				+ valorAplicado + ", rendaMensal=" + rendaMensal + ", resgateUnico=" + resgateUnico + ", peculio="
				+ peculio + ", pensao=" + pensao + ", usuario=" + usuario + "]";
	}

	@Override
	public LocalDate getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

	@Override
	public double getValor() {
		// TODO Auto-generated method stub
		return this.getValorAplicado();
	}

}
