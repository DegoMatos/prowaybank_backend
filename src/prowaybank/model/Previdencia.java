package prowaybank.model;

import java.time.LocalDate;
import java.util.List;

public class Previdencia implements InterfaceInvestimentoExtrato{
	private int idadeIncial;
	private int idadeAposentadoria;
	private double investimentoMensal;
	private double quantidadeAnosInvestido;
	private double valorInvestidoAnos;
	private final double JUROS_ANO = 0.05;
	private final int ANOS_CONTRIBUICAO_MINIMA = 30;
	private double taxaDeJuros;
	private LocalDate dataInicial;
	private List<IAplicacao> investimentos;

	public Previdencia() {
		super();
	}

	public Previdencia(int idadeIncial, int idadeAposentadoria, double investimentoMensal,
			double quantidadeAnosInvestido, double valorInvestidoAnos, double taxaDeJuros, LocalDate dataInicial,
			List<IAplicacao> investimentos) {
		this.idadeIncial = idadeIncial;
		this.idadeAposentadoria = idadeAposentadoria;
		this.investimentoMensal = investimentoMensal;
		this.quantidadeAnosInvestido = quantidadeAnosInvestido;
		this.valorInvestidoAnos = valorInvestidoAnos;
		this.taxaDeJuros = taxaDeJuros;
		this.dataInicial = dataInicial;
		this.investimentos = investimentos;
	}

	/**
	 * Adiciona investimento a lista de investimentos
	 * @param investimento
	 */
	public void adicionarInvestimento(IAplicacao investimento) {
		this.investimentos.add(investimento);
	}

	public double getTaxaDeJuros() {
		return taxaDeJuros;
	}

	public void setTaxaDeJuros(double taxaDeJuros) {
		this.taxaDeJuros = taxaDeJuros;
	}

	public LocalDate getDataInicial() {
		return dataInicial;
	}

	public void setDataInicial(LocalDate dataInicial) {
		this.dataInicial = dataInicial;
	}

	public List<IAplicacao> getInvestimentos() {
		return investimentos;
	}

	public void setInvestimentos(List<IAplicacao> investimentos) {
		this.investimentos = investimentos;
	}

	public int getIdadeIncial() {
		return idadeIncial;
	}

	/**
	 * Define quantidade minima para efetuar a previd�ncia privada
	 * 
	 * @param idadeIncial
	 */
	public void setIdadeIncial(int idadeIncial) {
		this.idadeIncial = idadeIncial;

		if (this.idadeIncial >= 18) {
			this.idadeIncial = idadeIncial;
		} else {
			System.out.println("Idade n�o permitida");
		}
	}

	public int getIdadeAposentadoria() {
		return idadeAposentadoria;
	}

	/**
	 * Define que a idade da aposentadoria seja maior que a idade inicial e maior ou
	 * igual ao tempo minimo estipulado pelo banco
	 * 
	 * @param idadeAposentadoria
	 */
	public void setIdadeAposentadoria(int idadeAposentadoria) {
		this.idadeAposentadoria = idadeAposentadoria;

		if ((this.idadeAposentadoria > this.idadeIncial)
				&& (this.idadeAposentadoria >= this.ANOS_CONTRIBUICAO_MINIMA)) {
			this.idadeAposentadoria = idadeAposentadoria;
		} else {
			System.out.println("Idade n�o permitida");
		}
	}

	public double getInvestimentoMensal() {
		return investimentoMensal;
	}

	/**
	 * Define que o investimento mensal deve ser maior que 0.
	 * 
	 * @param investimentoMensal
	 */
	public void setInvestimentoMensal(double investimentoMensal) {
		this.investimentoMensal = investimentoMensal;

		if (this.investimentoMensal > 100) {
			this.investimentoMensal = investimentoMensal;

		} else {
			System.out.println("Valor informado n�o permitido");
		}
	}

	public double getQuantidadeAnosInvestido() {
		return quantidadeAnosInvestido;
	}

	/**
	 * Define que a quantidade minima de anos investido deve ser maior ou igual aos
	 * anos de contribui��o minima
	 * 
	 * @param quantidadeAnosInvestido
	 */
	public void setQuantidadeAnosInvestido(double quantidadeAnosInvestido) {
		this.quantidadeAnosInvestido = quantidadeAnosInvestido;

		if (this.quantidadeAnosInvestido >= this.ANOS_CONTRIBUICAO_MINIMA) {
			this.quantidadeAnosInvestido = quantidadeAnosInvestido;
		} else {
			System.out.println(
					"A quantidade de anos invstida � insuficiente " + "a quantidade minima de anos de contribui��o");
		}
	}

	public double getValorInvestidoAnos() {
		return valorInvestidoAnos;
	}

	/**
	 * 
	 * @param valorInvestidoAnos
	 */
	public void setValorInvestidoAnos(double valorInvestidoAnos) {
		this.valorInvestidoAnos = valorInvestidoAnos;
	}

	public double getJUROS_ANO() {
		return JUROS_ANO;
	}

	public int getANOS_CONTRIBUICAO_MINIMA() {
		return ANOS_CONTRIBUICAO_MINIMA;
	}

	public double getSaldoSemRentabilidade() {
		double saldo = 0;
		for (IAplicacao ip : this.investimentos) {
			saldo += ip.getValor();
		}
		return saldo;
	}

}
