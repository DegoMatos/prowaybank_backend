package prowaybank.model;

import java.time.LocalDate;

public class Aplicacao implements IAplicacao{
	
	private LocalDate data;
	private Double valor;

	public Aplicacao() {
		super();
	}

	public Aplicacao(LocalDate data, Double valor) {
		super();
		this.data = data;
		this.valor = valor;
	}

	@Override
	public LocalDate getData() {
		// TODO Auto-generated method stub
		return this.data;
	}

	@Override
	public double getValor() {
		// TODO Auto-generated method stub
		return this.valor;
	}
	
}
