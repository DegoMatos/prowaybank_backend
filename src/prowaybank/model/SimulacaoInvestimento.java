package prowaybank.model;

import java.util.HashMap;
import java.util.Map;

import prowaybank.enums.TipoInvestimentoEnum;
import prowaybank.enums.TipoTaxaEnum;

public class SimulacaoInvestimento {

	private TipoInvestimentoEnum tipoInvestimento;
	private TipoTaxaEnum tipoTaxa;
	private Double valorInvestimento;
	private Integer meses;
	private Double taxaRentabilidade;

	Map<String, Double> taxasRentabilidade = new HashMap<String, Double>();

	public SimulacaoInvestimento(TipoInvestimentoEnum tipoInvestimento, TipoTaxaEnum tipoTaxa, Double valorInvestimento,
			Integer meses) {
		this.tipoInvestimento = tipoInvestimento;
		this.tipoTaxa = tipoTaxa;
		this.valorInvestimento = valorInvestimento;
		this.meses = meses;

		// Preenche o map com as taxas de rentabilidade.
		this.taxasRentabilidade.put("lcPre", 0.0205);
		this.taxasRentabilidade.put("lcPos", 0.04);
		this.taxasRentabilidade.put("lcaPre", 0.025);
		this.taxasRentabilidade.put("lcaPos", 0.0305);
		this.taxasRentabilidade.put("prev", 0.05);
		
		this.valorRentabilidade();
	}

	/**
	 * Define o valor da rentabilidade de acordo com os dados da simula��o do
	 * investimento.
	 * 
	 * @return
	 */
	public void valorRentabilidade() {
		if (this.getTipoInvestimento() == TipoInvestimentoEnum.LC
				|| this.getTipoInvestimento() == TipoInvestimentoEnum.CDB) {
			if (this.getTipoTaxa() == TipoTaxaEnum.PRE) {
				this.taxaRentabilidade = this.taxasRentabilidade.get("lcPre");
			} else {
				this.taxaRentabilidade = this.taxasRentabilidade.get("lcPos");
			}
		} else if (this.getTipoInvestimento() == TipoInvestimentoEnum.LCA
				|| this.getTipoInvestimento() == TipoInvestimentoEnum.LCI) {
			if (this.getTipoTaxa() == TipoTaxaEnum.PRE) {
				this.taxaRentabilidade = this.taxasRentabilidade.get("lcaPre");
			} else {
				this.taxaRentabilidade = this.taxasRentabilidade.get("lcaPos");
			}
		}
	}

	/**
	 * Retorna o tipo de Investimento.
	 * 
	 * @return
	 */
	public TipoInvestimentoEnum getTipoInvestimento() {
		return tipoInvestimento;
	}

	/**
	 * Atribui o tipo de investimento.
	 * 
	 * @param tipoInvestimento
	 */
	public void setTipoInvestimento(TipoInvestimentoEnum tipoInvestimento) {
		this.tipoInvestimento = tipoInvestimento;
	}

	/**
	 * Retorna tipo de taxa.
	 * 
	 * @return
	 */
	public TipoTaxaEnum getTipoTaxa() {
		return tipoTaxa;
	}

	/**
	 * Atribui o tipo de taxa.
	 * 
	 * @param tipoTaxa
	 */
	public void setTipoTaxa(TipoTaxaEnum tipoTaxa) {
		this.tipoTaxa = tipoTaxa;
	}

	/**
	 * Retorna o valor de investimento da simula��o.
	 * 
	 * @return
	 */
	public Double getValorInvestimento() {
		return valorInvestimento;
	}

	/**
	 * Atribui o valor de investimento da simula��o
	 * 
	 * @param valorInvestimento
	 */
	public void setValorInvestimento(Double valorInvestimento) {
		this.valorInvestimento = valorInvestimento;
	}

	/**
	 * Retorna o n�mero de meses que o valor e investimento pretende ficar
	 * investido.
	 * 
	 * @return
	 */
	public Integer getMeses() {
		return meses;
	}

	/**
	 * Atribui o n�mero de meses que o valor e investimento pretende ficar
	 * investido.
	 * 
	 * @param meses
	 */
	public void setMeses(Integer meses) {
		this.meses = meses;
	}

	/**
	 * Retorna a taxa de rentabilidade da simula��o do investimento.
	 * 
	 * @return
	 */
	public Double getTaxaRentabilidade() {
		return taxaRentabilidade;
	}

	/**
	 * Atribui a taxa de rentabilidade da simula��o do investimento.
	 * 
	 * @param taxaRentabilidade
	 */
	public void setTaxaRentabilidade(Double taxaRentabilidade) {
		this.taxaRentabilidade = taxaRentabilidade;
	}

}
