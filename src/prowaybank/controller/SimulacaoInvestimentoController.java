package prowaybank.controller;

import prowaybank.model.SimulacaoInvestimento;

public class SimulacaoInvestimentoController {
	
	private SimulacaoInvestimento simulacaoInvestimento;

	public SimulacaoInvestimentoController(SimulacaoInvestimento simulacaoInvestimento) {
		this.simulacaoInvestimento = simulacaoInvestimento;
	}
	
	/**
	 * Retorna o resultado da simula��o do investimento.
	 * @return
	 */
	public Double simularInvestimento() {
		return simulacaoInvestimento.getValorInvestimento() * Math.pow((1 + simulacaoInvestimento.getTaxaRentabilidade()), simulacaoInvestimento.getMeses());
	}

	/**
	 * Retorna a simulacao do investimento.
	 * @return
	 */
	public SimulacaoInvestimento getSimulacaoInvestimento() {
		return simulacaoInvestimento;
	}

	/**
	 * Atribui a simulacao do investimento.
	 * @param simulacaoInvestimento
	 */
	public void setSimulacaoInvestimento(SimulacaoInvestimento simulacaoInvestimento) {
		this.simulacaoInvestimento = simulacaoInvestimento;
	}

}
