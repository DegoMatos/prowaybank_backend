package prowaybank.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import prowaybank.model.Investimento;
import prowaybank.model.InvestimentoExterno;

public class rendimentoInvestimento {
	
	Investimento investimento;
	
	/**
	 * Consulta no banco de dados todos os investimentos externos realizados.
	 * 
	 * @param todosInvestimentos
	 * @return ArrayList<?> 
	 */
	 public InvestimentoExterno[] consultaInvestimentosExternos(ArrayList<InvestimentoExterno> todosInvestimentos){
		 InvestimentoExterno[] retornoBanco = todosInvestimentos.toArray(new InvestimentoExterno[0]) ;
		 return retornoBanco; 
	 }
		
	/**
	 * C�lcula o rendimento total de todos os investimentos'
	 * 
	 * @param todosInvestimentos
	 * @return double
	 */
	public double rendimentoTotal(ArrayList<InvestimentoExterno> investimentosExternos) {
		double valorTotal = 0;
		
		for( InvestimentoExterno n : investimentosExternos ) {
			valorTotal += ( n.getValorInvestido() * n.getVariacao() ) / 100;
		}
		salvarRendimentoTotal(valorTotal);
		return valorTotal;
	}
	
	/**
	 * Salvando valor de rendimento total no banco de dados
	 * 
	 * @param valorTotal
	 * @return valorTotal 
	 */
	public double salvarRendimentoTotal(double valorTotal){
		System.out.println("Salvando no banco de dados");
		return valorTotal;
	}
	
	/**
	 * Calcula o rendimento de cada investimento particular e organiza os investimentos por m�s em ordem crescente.  
	 * 
	 * @param investimentosExternos
	 * @return
	 */
	public ArrayList<InvestimentoExterno> rendimentoParticular(ArrayList<InvestimentoExterno> investimentosExternos){
		ArrayList<InvestimentoExterno> rendimentoMesal = new ArrayList<InvestimentoExterno>();
		
		for ( InvestimentoExterno n : investimentosExternos ) {
			
			n.setRendimento(( n.getValorInvestido() * n.getVariacao() ) / 100);
			
			rendimentoMesal.add(n);
			 
			}																
		
		rendimentoMesal.sort((o1, o2) -> o1.getDataInvestimento().compareTo(o2.getDataInvestimento()));
		
		salvarRendimento(rendimentoMesal);
		
		return rendimentoMesal; 
		
	}
	
	/**
	 * A partir da chamada da fun��o ele retorna todos os investimento do m�s solicitado.
	 * 
	 * @param investimentosExternos
	 * @param mes
	 * @return Array de investimenro com m�s ordenado
	 */
	public List<InvestimentoExterno> rendimentoMesEspecifico(ArrayList<InvestimentoExterno> investimentosExternos, int mes){
		List<InvestimentoExterno> investimentoMesEspecifico = new ArrayList<InvestimentoExterno>();
		
		investimentoMesEspecifico = investimentosExternos.stream().filter(n -> n.getDataInvestimento().getMonthValue() == mes)
				.collect(Collectors.toList());
		
		return investimentoMesEspecifico;
	}
	
	/**
	 * Salvando valor de rendimento total no banco de dados.
	 * 
	 * @param investimentosExternos
	 * @return
	 */
	public ArrayList<InvestimentoExterno> salvarRendimento(ArrayList<InvestimentoExterno> investimentosExternos){
		System.out.println("Salvando no banco de dados");
		return investimentosExternos;
	}
	
}
