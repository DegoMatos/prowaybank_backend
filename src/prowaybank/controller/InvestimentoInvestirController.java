package prowaybank.controller;

import java.util.Date;

import prowaybank.model.Investimento;

public class InvestimentoInvestirController {

	private  Investimento investir;
	 
	
	
	public  InvestimentoInvestirController(Investimento investir) {
		this.investir = investir;
	}
	 
	 public String[] listarTodosInvestimentos( double idUsuario){
		 // retorno dos investimentos que ainda n�o foram listados
		 return investir.getInvestimentoGeral();
	 }
	 
	
	/**
	 * Realiza investimento do usu�rio no investimento escolhido
	 * 
	 * Confirma se o valor digitado pelo usu�rio � maior ou igual ao valor m�nimo
	 * permitido para investimento 
	 * 
	 * Consulta o saldo atual do usu�rio e confirma se
	 * o mesmo possu� saldo dispon�vel para realizar o investimento 
	 * 
	 * Realiza o c�lculo de valor estimado de acordo com a taxa de rendimento do t�tulo escolhido
	 * 
	 * Confirma se a data recebida � valida
	 * 
	 * @param t�tulo            nome do t�tulo do investimento
	 * @param valorMinimo       valor minimo de aplicacao
	 * @param valorInvestido    valor do investimento
	 * @param taxaIvestimento   taxa para calculo de rendimento
	 * @param dataInvestimento  data que foi feito o investimento
	 * @param tempoInvestimento tempo que o valor ficara investido
	 */
	
	 public void realizaInvestimento() {
		 
		 
		
		double saldo = 0; // pegar esse valor com outro m�todo futuramente
		boolean valorCorreto = valorCorreto();
		boolean consultaSaldo = consultaSaldo(saldo);

		if (valorCorreto && consultaSaldo) {
			investir.setValorEstimado(valorEstimado());
			investir.setDataInvestimento(new Date());
			// faz a chamada da fun��o para salvar os dados no banco
		} else {
		}
	}

	/**
	 * Realiza a verifica��o do valor investido com valor minimo
	 * 
	 * @param valorMinimo
	 * @param valorInvestido
	 * @return true para valor correto e false para valor incorreto
	 */
	public  boolean  valorCorreto() {
		if (investir.getValorMinimoInvestimento() < investir.getValorInvestido())
			return true;
		else
			return false; 
	}

	/**
	 * Verifica se o usuario possui saldo maior que o valor investido
	 * 
	 * @param valorInvestido
	 * @param saldo
	 * @return true para saldo positivo e false para saldo insuficente
	 */
	public Boolean consultaSaldo(double saldo) {
		if (saldo >= investir.getValorInvestido())
			return true;
		else
			return false;
	
	}

	/**
	 * Calcula o valor final estimado para o investimento
	 * 
	 * @param valorInvestido
	 * @param taxaInvestimento
	 * @param tempoInvestimento
	 * @return valorEstimado
	 */
	public double valorEstimado() {
		double taxa = Math.pow((1 + investir.getTaxaInvestimento()), investir.getTempoInvestimento());
		double result = investir.getValorInvestido() * taxa;
		return result;
	}
}
