package prowaybank.controller;

import java.time.LocalDate;
import java.util.List;

import prowaybank.model.Aplicacao;
import prowaybank.model.Aporte;
import prowaybank.model.Extrato;
import prowaybank.model.IAplicacao;
import prowaybank.model.Previdencia;

public class PrevidenciaController {

	public Previdencia prev;

	public PrevidenciaController(Previdencia prev) {
		super();
		this.prev = prev;
	}

	/**
	 * Cria a previdencia
	 * 
	 * @param idadeIncial
	 * @param idadeAposentadoria
	 * @param investimentoMensal
	 * @param quantidadeAnosInvestido
	 * @param valorInvestidoAnos
	 * @param taxaDeJuros
	 * @param dataInicial
	 * @param investimentos
	 * @return
	 */
	public Previdencia criarPrevidencia(int idadeIncial, int idadeAposentadoria, double investimentoMensal,
			double quantidadeAnosInvestido, double valorInvestidoAnos, double taxaDeJuros, LocalDate dataInicial,
			List<IAplicacao> investimentos) {

		this.prev = new Previdencia(idadeIncial, idadeAposentadoria, investimentoMensal, quantidadeAnosInvestido,
				valorInvestidoAnos, taxaDeJuros, dataInicial, investimentos);

		return this.prev;
	}

	/**
	 * Adiciona aplicação na lista de investimentos
	 * 
	 * @param valor
	 */
	public void fazerAplicação(Double valor) {
		Aplicacao aplicacao = new Aplicacao(LocalDate.now(), valor);
		this.prev.adicionarInvestimento(aplicacao);
	}

	/**
	 * Adiciona aporte na lista de investimentos
	 * 
	 * @param valor
	 */
	public void fazerAporte(Double valor) {
		Aporte aporte = new Aporte();
		aporte.setValorAplicado(valor);
		this.prev.adicionarInvestimento(aporte);
	}

	// extrato previdência (lista de aportes e saldo atual com redimento)
	/**
	 * Retorna o extrato com a lista de aportes e o saldo atual com rendimento
	 * 
	 * @return
	 */
	public Extrato extrato() {
		Extrato extrato = new Extrato(this.prev);

		return extrato;
	}

}
